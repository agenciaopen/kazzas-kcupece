function initialize() {
    'use strict';

    var tour = document.getElementById('tourimg');
    var tourtxt = document.getElementById('tourtxt');
    tour.addEventListener('click', function (e) {
        tour.classList.add('hidden');
        tourtxt.classList.add('hidden');
    });

    var slider = tns({
        container: '#blueprints',
        autoplay: true,
        autoplayButtonOutput: false,
        autoplayHoverPause: true,
        autoplayTimeout: 9000,
        controlsContainer: '.tns-controls',
        gutter: 2,
        nav: false,
    });

    var big_slider = tns({
        loop: true,
        container: '#gallery',
        navContainer: '#gallerynav',
        items: 1,
        navAsThumbnails: true,
        prevButton: '#gallery_prev',
        nextButton: '#gallery_next',
        mode: 'gallery',
    });

    var small_slider = tns({
        loop: true,
        container: '#gallerynav',
        items: 3,
        nav: false,
        autoWidth: true,
        controls: false,
        center: true,
    });

    var changeIndex = function (info, eventName) {
        small_slider.goTo(info.index);
    }

    big_slider.events.on('indexChanged', changeIndex);


    let opencode = document.getElementById('opencode');
    let chat = document.getElementById('form-chat-online');

    var validate = new Bouncer('#opencode', {
        disableSubmit: true,
        // messageAfterField: false
    });
    opencode.addEventListener('bouncerFormValid', function (e) {
        $.ajax({
            url: '/cadastro.php',
            method: 'post',
            dataType: 'html',
            data: 'nome=' + document.getElementById('iptNome').value + '&email=' + document.getElementById('iptEmail').value +
            '&ddd=' + document.getElementById('iptDDD').value + '&telefone=' + document.getElementById('iptTelefone').value,
            success: function(result) {
                console.log(result);
                var ret = hc_envia_mensagem(
                '32530',
                document.getElementById('iptNome').value,
                document.getElementById('iptEmail').value,
                document.getElementById('iptDDD').value,
                document.getElementById('iptTelefone').value,
                'lead open',
                '',
                '',
                'lead open',
                ''
                );
                opencode.submit();
                //  document.getElementById('msg').style.color = 'green';
                //  document.getElementById('msg').innerHTML = 'Formulário enviado com sucesso';
            }
        });
    }, false);

    // chat.addEventListener('click', function(e) {
    //    hc_chat(
    //       '32530',
    //       document.getElementById('iptNome').value,
    //       document.getElementById('iptEmail').value,
    //       document.getElementById('iptDDD').value,
    //       document.getElementById('iptTelefone').value,
    //       ''
    //    );
    // }, false);
}
document.addEventListener('DOMContentLoaded', initialize);
